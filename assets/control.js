const button = document.getElementById("control");
const input = document.getElementById("input");

const textRef = firebase.database().ref('text');

button.addEventListener("click", () => {  
  textRef.transaction(function(text) {   
    if (text) {
      text.value = input.value;

      input.value = '';      
    } else {
      text = {
        value: ''
      }
    }


    return text;
  });
});